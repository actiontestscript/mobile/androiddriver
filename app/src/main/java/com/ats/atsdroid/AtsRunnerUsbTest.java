package com.ats.atsdroid;

import com.ats.atsdroid.server.AtsWebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Objects;

import androidx.test.platform.app.InstrumentationRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AtsRunnerUsbTest extends AtsRunnerTest {

    public AtsWebSocketServer tcpServer;

    /* A REFACTOR : NECESSARY POUR LE MODE UDP USB */
    public int udpPort = DEFAULT_PORT;

    private static final Logger logger = LoggerFactory.getLogger(AtsRunnerUsbTest.class);

    @Override
    public void stop() {
        try {
            tcpServer.stop();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        super.stop();
    }

    @Override
    public void testMain() {
        super.testMain();

        try {
            /* A REFACTOR : NECESSARY POUR LE MODE UDP USB */
            udpPort = Integer.parseInt(Objects.requireNonNull(InstrumentationRegistry.getArguments().getString("udpPort")));

            // fetch available port
            ServerSocket serverSocket = new ServerSocket(0);
            int availablePort = serverSocket.getLocalPort();
            serverSocket.close();

            tcpServer = new AtsWebSocketServer(new InetSocketAddress(availablePort), automation);
            tcpServer.start();

            while (running)
            {

            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
