package com.ats.atsdroid.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

public class ApplicationInfo {

    private final boolean system;
    private final String version;
    private String icon;
    private final String activity;
    private final String label;
    private final String packageName;

    public ApplicationInfo(String pkg, String act, String ver, Boolean sys, CharSequence lbl, Drawable icon){

        this.packageName = pkg;
        this.version = ver;
        this.activity = act;
        this.system = sys;

        if (lbl != null) {
            this.label = lbl.toString();
        } else {
            this.label = pkg;
        }

        if(icon != null){
            this.icon = drawableToBase64(icon);
        }

        //prevent empty icons
        if (this.icon == null || this.icon.isEmpty()) {
            this.icon = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAAH0lEQVR42mNkoBAwjhowasCoAaMGjBowasCoAcPNAACOMAAhOO/A7wAAAABJRU5ErkJggg==";
        }
    }

    /* public void setChromeDriverActivity(){
        this.activity = "chromedriver";
        this.version = DeviceInfo.getInstance().getChromeDriverVersion();
        this.label = "Chrome - ChromeDriver";
    }

    public void start(Context context, UiDevice device){
        final Intent startChannel = getIntent(
                Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT,
                Intent.FLAG_ACTIVITY_NEW_TASK,
                Intent.FLAG_ACTIVITY_NO_ANIMATION,
                Intent.FLAG_ACTIVITY_NO_HISTORY,
                Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED,
                Intent.FLAG_INCLUDE_STOPPED_PACKAGES
        );
        context.startActivity(startChannel);
        device.waitForWindowUpdate(packageName, 3000);
    } */

    public boolean packageEquals(String value){
        return packageName.equals(value);
    }

    public String getIcon() {
        return icon;
    }
    public String getLabel() {
        return label;
    }
    public String getVersion() {
        return version;
    }
    public String getPackageName(){
        return packageName;
    }
    public String getPackageActivityName(){
        return packageName + "/" + activity;
    }

    /* public Intent getIntent(int... flag){
        final Intent intent = new Intent();
        intent.setClassName(packageName, activity);
        for (int value : flag) {
            intent.addFlags(value);
        }
        return intent;
    } */

    public JSONObject getJson() throws JSONException{
        JSONObject result = new JSONObject();
        result.put("packageName", packageName);
        result.put("activity", activity);
        result.put("system", system);
        result.put("label", label);
        result.put("icon", icon);
        result.put("version", version);
        result.put("os", "android");
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static Bitmap getBitmapFromDrawable (Drawable drawable) {

        if (drawable instanceof AdaptiveIconDrawable) {

            Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
            Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

            Drawable[] drr = new Drawable[2];
            drr[0] = backgroundDr;
            drr[1] = foregroundDr;

            LayerDrawable layerDrawable = new LayerDrawable(drr);

            int width = layerDrawable.getIntrinsicWidth();
            int height = layerDrawable.getIntrinsicHeight();

            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);

            layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            layerDrawable.draw(canvas);

            return bitmap;
        }else if(drawable instanceof BitmapDrawable){
            return ((BitmapDrawable)drawable).getBitmap();
        }
        return null;
    }

    public static String drawableToBase64 (Drawable drawable) {

        Bitmap bitmap;
        if (Build.VERSION.SDK_INT >= 26) {
            bitmap = getBitmapFromDrawable(drawable);
        } else {
            bitmap = ((BitmapDrawable)drawable).getBitmap();
        }

        if (bitmap != null) {
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 32, 32, true);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

            /* if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }

            if (!scaledBitmap.isRecycled()) {
                scaledBitmap.recycle();
            } */

            return Base64.encodeToString(outputStream.toByteArray(), Base64.NO_WRAP);
        } else {
            return "";
        }
    }
}