/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.atsdroid.utils;

import android.app.Instrumentation;
import android.app.UiAutomation;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ListView;

import com.ats.atsdroid.AtsRunnerTest;
import com.ats.atsdroid.AtsRunnerUsbTest;
import com.ats.atsdroid.element.AbstractAtsElement;
import com.ats.atsdroid.element.AtsRootElement;
import com.ats.atsdroid.response.AtsResponse;
import com.ats.atsdroid.response.AtsResponseBinary;
import com.ats.atsdroid.response.AtsResponseHTML;
import com.ats.atsdroid.response.AtsResponseJSON;
import com.ats.atsdroid.scripting.ScriptingExecutor;
import com.ats.atsdroid.server.RequestType;
import com.ats.atsdroid.ui.AtsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.Configurator;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.UiSelector;

public class AtsAutomation {
    
    private static final int swipeSteps = 10;

    private final Instrumentation instrument = InstrumentationRegistry.getInstrumentation();
    private final UiAutomation automation = instrument.getUiAutomation();
    private final Context context = instrument.getTargetContext();

    public final DeviceInfo deviceInfo = DeviceInfo.getInstance();

    private final UiDevice device;

    private final List<ApplicationInfo> applications = new ArrayList<>();
    private AtsRootElement rootElement;
    private CaptureScreenServer screenCapture;
    public Boolean usbMode;
    private int activeChannelsCount = 0;
    private AbstractAtsElement found = null;
    private final AtsRunnerTest runner;
    public int port;

    public AtsAutomation(
            int port,
            AtsRunnerTest runner,
            String ipAddress,
            String serial,
            Boolean usb) {

        this.usbMode = usb;
        this.port = port;
        this.runner = runner;
        this.device = UiDevice.getInstance(instrument);

        sendLogs("INIT START\n");
    
        Configurator.getInstance().setWaitForIdleTimeout(0);
    
        sendLogs("WAIT FOR IDLE TIMEOUT\n");
    
        deviceInfo.initDevice(port, ipAddress, serial);
    
        sendLogs("DEVICE INFO INIT\n");
    
        //-------------------------------------------------------------
        // Bitmap factory default
        // ------------------------------------------------------------
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        //-------------------------------------------------------------
    
        sendLogs("BITMAP YO\n");
    
        SysButton.pressButton(SysButton.ButtonType.home);
    
        sendLogs("PRESS BUTTON\n");
    
        launchAtsWidget();
    
        sendLogs("ATS WIDGET\n");
    
        loadApplications();
    
        sendLogs("LOAD APPS\n");
        
        deviceSleep();

        sendLogs("ATS_DRIVER_RUNNING\n");
    }

    public static void sendLogs(String message){
        Bundle b = new Bundle();
        b.putString("atsLogs",  message);
        InstrumentationRegistry.getInstrumentation().sendStatus(0, b);
    }

    private void launchAtsWidget(){
        final Intent atsIntent = new Intent(context, AtsActivity.class);
        atsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        atsIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        atsIntent.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        atsIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        atsIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(atsIntent);
    }

    public void reloadRoot() {
        AccessibilityNodeInfo rootNode = automation.getRootInActiveWindow();
        while (rootNode == null) {
            wait(200);
            rootNode = automation.getRootInActiveWindow();
            AtsAutomation.sendLogs("root node is null\n");
        }
        rootNode.refresh();

        try {
            rootElement = new AtsRootElement(rootNode);
        } catch (Exception e) {
            AtsAutomation.sendLogs("Error on reloadRoot, retrying:" + e.getMessage() + "\n");
            wait(200);
            reloadRoot();
        }
    }

    public void hideKeyboard() {
        // use application level context to avoid unnecessary leaks.
        SysButton.pressButton(SysButton.ButtonType.back);
        wait(500);
    }

    public void enterKeyboard() {
        // use application level context to avoid unnecessary leaks.
        SysButton.pressButton(SysButton.ButtonType.enter);
        wait(500);
    }

    public AbstractAtsElement getElement(String id) {
        found = null;
        getElement(rootElement, id);
        return found;
    }

    private void getElement(AbstractAtsElement parent, String id) {
        if (parent == null) { return; }

        if (parent.getId().equals(id)) {
            found = parent;
        } else {
            for (AbstractAtsElement child : parent.getChildren()) {
                if (found == null) {
                    getElement(child, id);
                }
            }
        }
    }

    public JSONObject getRootObject() {
        return rootElement.getJsonObject();
    }

    public ApplicationInfo getApplicationInfo(String pkg){
        return getApplicationByPackage(pkg);
    }

    private void loadApplications() {
        applications.clear();

        final PackageManager pkgManager = InstrumentationRegistry.getInstrumentation().getContext().getPackageManager();
        final List<android.content.pm.ApplicationInfo> apps = pkgManager.getInstalledApplications(PackageManager.GET_META_DATA);

        for (android.content.pm.ApplicationInfo info : apps) {
            final String pkg = info.packageName;
            Intent launchIntent = pkgManager.getLaunchIntentForPackage(pkg);
            if (launchIntent != null) {
                ComponentName componentName = launchIntent.getComponent();
                if (componentName != null) {
                    ActivityInfo[] activityInfos;
                    try {
                        activityInfos = pkgManager.getPackageInfo(pkg, PackageManager.GET_ACTIVITIES).activities;
                        if (activityInfos != null) {
                            for (ActivityInfo activityInfo : activityInfos) {
                                if (componentName.getClassName().equals(activityInfo.name)) {
                                    final String act = activityInfo.name;

                                    final ApplicationInfo app = getApplicationByPackage(pkg);

                                    if (app == null) {
                                        String version = "";
                                        try {
                                            version = pkgManager.getPackageInfo(pkg, 0).versionName;
                                        } catch (PackageManager.NameNotFoundException e) {
                                            AtsAutomation.sendLogs("Error, cannot get version name:" + e.getMessage() + "\n");
                                        }

                                        applications.add(new ApplicationInfo(pkg, act, version, (info.flags & android.content.pm.ApplicationInfo.FLAG_SYSTEM) != 0, info.loadLabel(pkgManager), info.loadIcon(pkgManager)));
                                    }
                                }
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        AtsAutomation.sendLogs("Error, cannot get version name:" + e.getMessage() + "\n");
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------

    private ApplicationInfo getApplicationByPackage(String pkg) {
        for (ApplicationInfo app : applications) {
            if (app.packageEquals(pkg)) {
                return app;
            }
        }
        return null;
    }

    private void executeShell(String value) {
        try {
            device.executeShellCommand(value);
        } catch (Exception e) {
            AtsAutomation.sendLogs("Error execute shell command:" + e.getMessage() + "\n");
        }
    }

    public void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignored) {}
    }

    //----------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------

    public void clickAt(int x, int y){
        clickAt(x, y, 1);
    }

    public void clickAt(int x, int y, int count) {
        while (count > 0) {
            try {
                // Obtenir la classe UiDevice
                Class<?> uiDeviceClass = device.getClass();

                // Obtenir la méthode getInteractionController()
                Method getInteractionControllerMethod = uiDeviceClass.getDeclaredMethod("getInteractionController");

                // Autoriser l'accès à la méthode si elle est privée
                getInteractionControllerMethod.setAccessible(true);

                // Appeler getInteractionController() pour obtenir l'instance d'InteractionController
                Object interactionController = getInteractionControllerMethod.invoke(device);

                // Obtenir la classe InteractionController
                Class<?> interactionControllerClass = interactionController.getClass();

                // Obtenir la méthode clickNoSync(int x, int y)
                Method clickNoSyncMethod = interactionControllerClass.getDeclaredMethod("clickNoSync", int.class, int.class);

                // Autoriser l'accès à la méthode si elle est privée
                clickNoSyncMethod.setAccessible(true);

                // Appeler la méthode clickNoSync(x, y) sur l'instance d'InteractionController
                clickNoSyncMethod.invoke(interactionController, x, y);

                wait(150);
                count--;

            } catch (Exception e) {
                // Gérer les exceptions
            }
        }

        wait(350);
    }

    //----------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------

    public void press(int x, int y, int duration) {
        swipe(x, y, 0, 0, duration * 50);
    }

    public void swipe(int x, int y, int xTo, int yTo) {
        swipe(x, y, xTo, yTo, swipeSteps);
    }

    public void swipe(int x, int y, int xTo, int yTo, int duration) {
        device.swipe(x, y, x + xTo, y + yTo, duration);
        wait(swipeSteps*6);
    }

    //----------------------------------------------------------------------------------------------------
    // Driver start stop
    //----------------------------------------------------------------------------------------------------

    private boolean driverStarted = false;

    public String startDriver() {
        driverStarted = true;

        deviceWakeUp();
        executeShell("svc power stayon true");

        launchAtsWidget();

        if (!usbMode) {
            screenCapture = new CaptureScreenServer(this);
            (new Thread(screenCapture)).start();
        }

        return UUID.randomUUID().toString();
    }

    public void stopDriver() {
        if (driverStarted) {
            forceStop("ATS_DRIVER_STOP");
        }
    }

    public void forceStop(String log) {
        driverStarted = false;

        sendLogs(log + "\n");

        executeShell("svc power stayon false");
        launchAtsWidget();

        if (screenCapture != null) {
            screenCapture.stop();
            screenCapture = null;
        }

        deviceSleep();
    }

    //----------------------------------------------------------------------------------------------------
    // Driver start stop
    //----------------------------------------------------------------------------------------------------

    private void deviceWakeUp() {
        try {
            device.wakeUp();
        } catch (RemoteException ignored) {}
    }

    private void deviceSleep() {
        try {
            device.sleep();
        } catch (RemoteException ignored) {}
    }

    public void terminate() {
        runner.stop();
        executeShell("am force-stop com.ats.atsdroid");
    }

    //----------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------

    public void switchChannel(String pkg) {
        if (pkg == null) {
            device.pressHome();
            reloadRoot();
            return;
        }

        AccessibilityNodeInfo rootNode = null;
        while (rootNode == null) {
            rootNode = automation.getRootInActiveWindow();
        }

        if (pkg.contentEquals(rootNode.getPackageName())) {
            return;
        }

        final ApplicationInfo app = getApplicationByPackage(pkg);
        if (app != null) {
            device.pressHome();
            executeShell("am start -f 536870912 " + app.getPackageActivityName() + "\n");
            reloadRoot();
        }
    }

    public void stopChannel(String pkg){
        stopActivity(pkg);
        launchAtsWidget();
    }

    private void stopActivity(String pkg){
        if (pkg != null) {
            executeShell("am force-stop " + pkg + "\n");
        }
    }

    //----------------------------------------------------------------------------------------------------
    // Screen capture
    //----------------------------------------------------------------------------------------------------

    public byte[] getScreenData(int rotation) {
        // 1. Get channel and navBar size
        deviceInfo.setupScreenInformation();
        
        // 2. Redraw bitmap
        int x = 0;
        int y = 0;
        
        Bitmap screen = getScreenBitmap();
        int width = screen.getWidth();
        int height = screen.getHeight();
    
        int navBarInset = deviceInfo.getNavBarInset();
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (Objects.requireNonNull(manager).getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            height -= navBarInset;
        } else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    height -= navBarInset;
                    break;
                case Surface.ROTATION_90:
                    width -= navBarInset;
                    break;
                case Surface.ROTATION_180:
                    break;
                case Surface.ROTATION_270:
                    x = navBarInset;
                    width -= navBarInset;
                    break;
            }
        }

        offsetX = x;
        
        screen = Bitmap.createBitmap(screen, x, y, width, height, deviceInfo.getMatrix(), true);
        return getBitmapBytes(screen, Bitmap.CompressFormat.JPEG, 66);
    }

    public int offsetX = 0;

    public byte[] getScreenDataHires() {
        return getBitmapBytes(getScreenBitmap(), Bitmap.CompressFormat.PNG, 100);
    }
    
    private Bitmap getScreenBitmap(){
        Bitmap screen = automation.takeScreenshot();
        if (screen == null) {
            screen = createEmptyBitmap(deviceInfo.getChannelWidth(), deviceInfo.getChannelHeight());
        }
        return screen;
    }

    private byte[] getBitmapBytes(Bitmap screen, Bitmap.CompressFormat cf, int level){
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        screen.compress(cf, level, outputStream);
        screen.recycle();

        final byte[] bytes = outputStream.toByteArray();
        try {
            outputStream.close();
        }catch (IOException e){
            AtsAutomation.sendLogs("Error on Stream close\n");
        }
        return bytes;
    }

    private Bitmap createEmptyBitmap(int width, int height) {

        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        bitmap.setHasAlpha(false);

        final Canvas canvas = new Canvas(bitmap);

        final Paint paint = new Paint();
        paint.setARGB(255, 220, 220, 220);
        canvas.drawRect(0, 0, width, height, paint);

        rootElement.drawElements(canvas, context.getResources());
        return bitmap;
    }

    private void releaseDriver() {
        activeChannelsCount--;
        if (activeChannelsCount == 0) {
            AtsClient.current = null;
            sendLogs("ATS_DRIVER_UNLOCKED\n");
        }
    }

    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------

    private final static String EMPTY_DATA = "&empty;";
    private final static String ENTER_KEY = "$KEY-ENTER";
    private final static String TAB_KEY = "$KEY-TAB";

    private ApplicationInfo appInfo;

    public AtsResponse executeRequest(RequestType req) {

        if (RequestType.STATUS.equals(req.type)) {
            String atsVersion = "";
            if (AtsClient.current != null) {
                atsVersion = AtsClient.current.version;
            }

            final String htmlContent = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /></head><body><img src=\"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAARAAAAA7CAYAAACzFtVUAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gQFFCUzGQr8hgAAIABJREFUeNrtvXl8VFW2NvysfU7NVZkTCEMgQASZZBIBAQFnVBTnEQfUdtbuvvfttu3bc/v2vW+3fZ26W1scEFFREEURZLJFQUBA5nnKnJC5UuM5Z6/vjzoVKkVVUglg2/1l/6hfSOUMe1jr2Ws9e+21CWemUCfvY3SVrtJV/mUKfc+f1wUoXaWr/BsDCJ2B53IXmHSVrvLvDSDUxnfUwXdwO99xJ0GkM+/tKl2lq5xhAEkEEBT3PXXi+Zziz7aUn/4Jfcj/hDp0gV9X+ZcEEEoAHPEgQgkAhFJQhFigiP0gwe+pKO7/XyyQLjDpKv+0op4ieNCIESPU666/Pq1f/369FEWxGIZB0Y80DGIGJEv4/b7ILcwRJOCI7EvJsqSkuGrtF1/Ub9m8JZwAQGQShWmPf0kEXnwalLEjz6PvADCoC0y6yvfdAkkEHuLd9xYMyMrKukMIMY6IHAAEADAzmT8BALphwNfcbEo4gyWbks5gZmbJmq7ru0uKi9988qdPbjdBQ8YASOz/uR0XJpk71Za7hA6C1Gkp+b16U0VpCaf6fZI6t/d7V+kq/1QASQgeHy/9ZLrL5XoSgD0WNKSUkeuZIZmJARi6jmafCSCmBQLmCMAwAWAQETMYjQ2Nrz/5f346t76hXosBDyMOQGQiRb/vif/8LYDxABQAYMk+aejL132xZs7eHdu0FPiW76S/73nkh3cIIa595bk/zYz94+zHfvR/CDT0lef+NCtFd6+tNnQBSVc540V0BjyWfrr0OpfL9WsAdmYmZiYpZcvHkAYZ0Z+GToaUJKV5DUtiQxJLJjCIEflON3QyDIPcbtfdv//D0w+np6fbTBdLMT/C/FDMz5bP+MlTLczsMQxjccnRwzeVFx+73Rdo/rNQ1ekDBw/tHfcMAUC409Ki3ymJ/t7GR0nLyDjp+kTfxTy75W9M1ENK+XXsdXnd81WAMqQ0NiR7Z9zvyXinrtJVvhccSELOY978t3o7nM5HolYHRywJklKSrus1hmH4SFCGECKDJRODYUgD0jBaHialDIS18HFFCJuiKN2YAaIoLwK43e6Zd95158rnnn1ufwIfP+ESssvtsQDIDQT97y5bvLABgLjm5juqnA4X+5q9oRvvuvdclyftHoVEIYMrfc3ev7/z6stfzX7sxysqyktnLX3/3fI7H3zsLqvVeu9br/x10sVXzOyZm9/95fra448pVos9zZ3+OIBeUvK2wwf3Pv35sqVNsx/90R+klKWCRC8G06sv/Pkndz/8xAOKqlwBRjgc1uZ9/P78DwcNOcc1cNjw/1QUMVbTtMWC0DscCn1mggIDgKKqCgE5kuG/97H/eA8kNa+3+YV3X315wz0PP/FzoSi9X3nuT/ePmzTFMXTUmJcNw1j+2gt/fj0F96vLEukq3zmAJCIiCYDIycmZycyuKHhIZmIp6eixo89/sHDRhoryCqNbfnfHjTfeeHVet24zwUyQEswSDKC+vn7tvLlvzq2trQva7XYxZcoFheeOHfsUmXVhZoCgDh0+9DEAjydRgnhilQw2LAAcLofr4dmP/Vias7RbN/TtVoc925OW/lQ4HFy2b9+eX/QrOvsql9vzQK8+fTczy71prrT+Uy6brqkWyyUM1BmGVDKys2dq4fDy5qbmUK++fX9TX1f3zKG9u/aNHHf+Twv7Fz30OfD/GOhLQilsqK99esUni/fMeuCxu8Gc+eXqz28tGnR297z8bv9dOGDgyqLBg28UgvofOXzovt4FfR8GqF9NVfXuGAuQrTabCqIsoSi2Y0cP/rhnr4Ib3S73gwA2GdKohhD5fQuLbL379x/EzPzaC3+eZ97PbbgvXeDRVb5zF+Yk0Ij9KIoyTNf1Ot3Qj+uaVmsYesO+/fv/8IN771++7NNl3m3btoU+W7a86d57Zr9VVlKyCAAMyWQYkrxN3l2PPvTIixu+3uA9eOCAtnPHjsALz7+w++MlS54Mh8PVYS1cpxl6rWbodUQi/Zbbbu2TontB2Xl5+QAUXde/1rTwlnA49JW3oeGvb7z47NPZOXnTpWEUL5r7xhvr1qxumvfyi/MAdEvPyLQbul6hWC19evTuc4kW1rYyszp46DlpiqKMXvDG6y9l5+VNBLOakZX19OgJkxYLIUYQqE9ausdKRG5fY8PLC+e9vqepvoFVq+UyoaoTzp865cO8/G5/AWD1eZtIUdSL6+rqXli1ZHFVQ13tewBo5dIPK2LapRQNHtIdQGZ1WdkzKz5aXLVvx+75IHQDoOia0QzA2uzzCo87/a7mxqa/JuoDpL583lW6yhmxQE6K63j3vQX90tPT777skkt/DUD87KdP/kgyq+FwSNHCmqJpmnro0CEJQBk5cvTInJzsO+vr6z/85ptN63//+98vfuZ//zxVCMpmZsx9442XACgul8syatTom6Xk4OZvNr23aOGiklUrVz2uKApICEMQSRLCaGxoCAMQf3r2mdvr6+q3/e7Xv90aU7/YFRlyOz1DWRrFr//l2TnxSiVIyWBmn8/fDAB0/ay7L2Dm+kAgAE3TKlSLpUhRlMLF8998+Nrb75w2fMzYR/x+7/KAv4lAbGdweP6cv83MyelmaWqs13VN46mXX3k2MweOV1cdOQHE0lNZXv7k1g1fHwGAhroaw+/zyfOnXZx9aN/uIwAUt9t9npRGpcVqVTVNi5LDyMnJG8bMtGThOwcBiIIB/SYZhjwGQNG0cLPVbtWnTZ9+EcDOzRu+3BlrvZgkM8W4eF2WR1f5zgGkFXj84pe/cIyfMGGWxWK5Tzf0b6Oz5aZNm4wEoKMMHDiooFu3br8BQDk5uT8ePHjIU4cPHzocCATKHQ5HtjSM0Pp162sBYMyYc2fb7Y7JADB6zLn81Vdr366rq4tfro2+RwHg7lvY99kX/vriJ8uXLfvLkg+XNMSACAAwKWKoIY0jCWZi8jc3rvNkZN1/670P3MKS2e5yXRUKBD8vPnJIGzF2XFWGw3lVMBhYU19XKwGwUJWzv/h42QsARN3x4zt69O4z87pb7pqtG1qty+25zttYPxcgZmZ/eVmJL6rM4VB4TV73/EcnXXjJUpvdPlhR1IHvvPrSQ1IaxSPHnPejwcNH7LFYbdMM3dgTDocpxgWBxW4bAuaGux56/FehYGi30+W8rrGh/u8ARDgcanaTJ93lSb/Z29j09sG9e7QYAIm6a7ILPLrK94YDmTh50jNEdK5kSVJKBqDk5Oaos2bN6qEoiiUUCinBYFCtqa3VFr2/sF4IYY3lKBRFUe12OxGRA8wgIhsACKEoQiju6IWKoroB4PxJE7PTPB4rEbGiKgYz5PZt248XHzvmN3QDzAyb3XbFVVfPuNzvD0xftWKlL0ZhWAgxNBgMzU9ktr/35uurr7v1TunJyroRYNHc2PjOe3PnfAIA4WCwEQAd2LVjSUQRyVFZVvp0eWmJBkAsW7xw36XXXP90fs+e91tgoerKyuc/fm/+N7fd99D9LKV2cPcuf1SZV32yZM6F06+Y7XS7btY1Y+/qz5b+RyDgR2nxsb/0Kuj7mCC1DuBG3dDqdU1r5WJYVHWoz+dbaFGV3nan8wpfc/N777/52j8AUCgUbALIJaUsf2/unDUxriUjcdwLt/F7V+kqp60kJUuXr/xsPhEVSWZoodC3V11x1Q8vuewS9+OPP/4OM9IihKckw5D+//r5z+/ZsX1HeNy48Re7XO4Jfr9/2/r1Xy29ZuY1PW+9/bY/SsNQ/X4/9u7dN+d//vDfqwcMKOpTUFBwKzPChw8feqOxsaH6//7PH55XFJEdeW7Eqti+bdsvX3z+xc3//cf/uT0rO+uGSKwJ80eLP7ru448+rjVnXWmz2xEKBjmuDcDJy51A8iCzzvAFqYTWtxXGzwDYYrFA07R4pScAuHX2A7eREBk2m21CeWnx75ctXrg7zkqL/Rn76ewmxK7SVU7JAiEAJKWUJARYSpKRIDElEAiqhiHDJKhlCRdg15M/e+pXv/31r3/39dfrVwNYA4AmTZqUdeNNNz7FzKpEJHhsQNGAW2deN/PoBws/OHzw4IGno4rwm9//dpaiiGxmgFkCTGBIGQqHAUAYhiGklEQgMEC+Zj+Zrk1k9rZYyGK1tlYSZmIGmaBDzCAzag2R/0c8nyRK3wkAoTgl5VYAEn0nUewKCXHkd2LVYqXYe5lBwYCfLVbbeSSg1tfVzlm2eOFeu8OhEJEM+P0yxfp2gUdXOaMAQglm7gg4cCTsXBqSAAgtFFYMaRABBElRJYXdbhn8X7/8xR+rq6s3NDd7az1pnp45ubnjhaJmciRcHYY0iMHOSy+79MkxY8/9pq6m9rDNbnfl5eWNstlt/Q0jMmkyIv+YmQxdFwAUaRgiEtkqCURsSF3EAAhde/tdT1gs1osiCkocjYpNwfL6vpSk+1uYOQSAM7NzHpv92I9/aP7R+cpzf5ryHYAFdbL+/24WehcId9ACicRjGJJi+k8xDENIwyCFFZIc4S+lNChiBag98nvkX8voHhX8SOwHM1jKaMAZQOTKysq6ICsr6wJzrwyzlK2nchO8DENGAERKMqSMABcAXdOVGA6A5/71+T8D+BNaxz8kc8+QgjvTWcXnDjwzfrcxErkwSZ6RaE8Qf8eg0d49/C8IFP8K7fte9XXyQDKGCwRIbnFhhG4YIsKnRlxwlpKkyYXwiRkzAhhREGJASoaUsuXvpo0R3ZnbsneGTRuEGMySSZoAYkgpzK9N4lWxoPXKA7VBFnICBeckANLRPCNtvS/Ze9oDD8SRo/GgxEk+pypMdIpgkggE+V8YONrb5fxdWiepAhx/XwAE9Q31L2VmZj4FwBndVcvMJFmCGOZGOW61iS56HaJchkmISjYjUSUIUSeFonvqGLE/EdmpS8wM3dAUAApH3kUAZN3xmg/WfrG2IcFMjSQKFy8E/B0IeHw92ooUbW9nMZIASDxhyp1sT1s8UCoBaW1t4qPviYJxJ9udTEH5O1Re6sS1/B31YXIAuf2W21b//Bc/PzBy1KgfElFO5BEShjSIBIHBLTtvpZQUAY0IqIAhGdwCKNIwIA0zAYhpaRBAJ6wZc+UlzjIxDEMAEEIIuxYKF69f//Wz7779zv42lK0j+VTlaeREqA0rJJEipbIbmNoxWc8UeCRz86gNS4nbaOOZVDA6DddRG5NRe209k2DZUYuws6BGpwJOalv++e9+87syAE/+7unfDwMgQ+GwQUQyymcwnwCRqCXi9/vLfzD7/qcSKEirij79h6f/y+3x9DfxIpY+RdRVMd0e+nLtlytWfrZiDgA93nR3p6WJQUOHpwOQiqJKRBCHvU2NoZ1bN/vbcB1OB1In6lQqLBpo69GrtzPaN3F14RTdmPZcp1hLhNsAkpQE++a7709jZquua6TrOhm6Toaht7zfMIxWdVEUhSMGo+TlixfVJbHsTnWmptNwLbdzD40aN8HSvUdPm1BVK0upaKEw65pmNDTUhbd8vS6UgtXIKbo9nWrDpTOuteZ06+7RNS2iYz6fvuS9+d4ULUCc5j5sJU9qEoRtJaA//9lTOwFY9uze49fCWr3Fask1wcPM62EqfsRfEekZ6bbGhsZw3Mtt0RelpaWBGYKlbMkPIrkFiCCIWNP15gP7D1QCwMrPVpQlMN0ZAF91w83X2h3O8TgRuQoAQtPC3+7cuvk1tJ8qsTOuSSs35fo77hn92ZKFW5saGhkAj79g6jS7w3mpGdBCwYB/6c6tm5cnEcSOgkhb7Uml3ic9d+R5ExR3WtrDAPqa5iCZ5mLMalDrepjL0WQY8iiAP8aBGVIU6lMR9pPA/K6Hnxi7cd2aLbu3btNTqAMBwN2P/HC6EGIUgDQAToDVCMGHEDN7h48aU7zr2y2LNq37srkd/ulM5MYlAOjRu2Cioqq3mjIuXB73PwC8k8Bd7szk2LoPH3pizPovV27Zt32nTIXfU5P46rHKGt1nYQCg4zXHV/TI71EUI1hmTIiM6Isg28zrrh0cDARDRARBxIY0KOAPWIhOxEooimKX3JKXrCV6jEwQaW7y7t2+bXtdgrq0+P7jL5jazWZ3jjOXOlsVQUo3nFipkZ1Q3DbBw+500oXTr8rv1j1/tmHIhqaGxs3RfrRarP0ARBMYqU3epsPtgBi34x60xTdwG/xLKgpKhUVFNgAuAFo0bqYFNDgWRNiMZWnxTtVwMLg9pp9j3atUidZTMe8JAG64c3b39IyM2VIy7d667Zu4+iS8f/CIEer4ydMeIhID2Qw+MkHSMNtsA2ATipozdNS5IzOzuz3z2ZKFpUieavNUSeyk7RNCDAYQNuMTrIFAYB9ab4RtbxJp00Xu1qsXTbn48p6etLT7pJQN+7bv3JKgDxPus1LbEFoZ86IoiODjjz5edffsu68VQuSzZEiWkZUWU6IsFkvmpMmTHmUzcss0fxHwB5hjyVIwgZm5hU4lliwJEcvGv+DdBfNiQCM+0tKwqCr3Kxo4DeBwXAvIrLW1R+8+9vKSY8EkHAl3cCZsAY8JF1zkOmvokDsVofQDwR70B9bG3keqOhyADiIQYNuw/ouj7bgiSGHmTkU4qYNCBFNAbQB6RAwLEIPIDBT04QSBzUTkagH8CJBYy0pL9sYJm0TrNAMdEexUAvtaJrzJ06Y7ioYMuhNEZ4Nh1zT9owSKlUjRafzkCx8goljwsACwgDmao0VnZs2c2ETPgt6PTbnsymc+X/ZxNVpnxJNnkPeIyK0Qo8FMEfSGY/Pm9fvRejNl9FrZhrWMBHQCXTbjBnfPPr3vAtEAMOxhTf8iwbNlMh5ITfDQRFZISyXWrFpdP2LkiKdHjhr5OyJKJwYkZKT2kcRCLA0ZYpaSSHAkZ4hk3Wi1eRZEcBCIImMXXQZmliz1nTt2vbFv7776ONDQY8Fk+NjxGRabfTjARhQ4IsJgNp5hzczOdpWXHAsn42HamfmTlkHDh/0HEWVyhK5Ry8uK90QH22qz02vPP/NAO8KR6ix1qiQYpQJC7899rQnA/YjLfnbXI0+8AHDIBGXbay/+748SyAbjxJK6SEBQd9QqSnXJkgYMHfQTAuVxRCaU48crduFEkqboe06yRm68697uAPrFgIds9jYt+cfK5eurSkt0q92uXDJj5ois7NwrhIiAJoFs+T16TgSwOI6IFx0g5LkDwAEA5EnPEHOe/eMPcGJbRvTvIoFVG/8dtTNBUo++vX8CUKbZBqWivGRvXB9yXBtbPUtto5HxQhD1K+nPf3xm9+z7Zv9k3ITxP7Ko6sBoCDozs65pDWtWr/lbVWVVvaqqIBEJFTEMKYmIiUCCBF108YU/VVRLDjMTE7M0DNI1vXbjho1///CDD3fFgIURBx4GADlk+Ih7T+yagRIOh/YTkUVR1UKznlaXy+0E0JigU1OJ1Ug4yJ6MDIWIcpmhgZkk2L92+ad1AITFahXhUDAZUMWjdyrgQR0w/dvjSiiJSZrwOdOvvaE/RQxGYmZF17WSBAKKBMBBbVggqXIGKeXqFaA8BktmCAkOLlv4XjUAxWZ3IBQMxCflbmmzoqiZACxRt0ULh3YteP2V6MyrhINB+njB29umXX6Vt6Cw3/0RVohhsVpHAvgowWqeQOIwgY5MTAmDG72NDYlSeCYj1NGG5XdSf2fkZAsBymWwZvZhYNVHi+vjAEQmwQRORqK2BSJRopLm/H3OwXVfrXty5nUzJ/bs1etyQZTOkimsa77t23YU79m924skG7y6detmm3jBxHJVypCUUhiG7q+sqPz6008+XXf0yFFvHHDo8dbH1bfcPlxRlCwGR+snP1n0/luXXz3zFkVVo26MzeZwuBMId8tnwgUXuQrPGjjYZrOeTyT6ENgmGY26oe2sqa5atXThgqpow4eNOtc+duLkv5i+qB/MIuJ4Qbn7kR/+laX0vv6XZ392230PPWi12YZEwI0sNdWVLy5Z8PaOmMEQF185s3tefv4om9U6FIJyQaSAZZPU5Z76upqvPnx3/pH4Fawrrr85N79Hz99E2yOZS9atWvH/ho0ZU+h0ec5TVDEYpKSR5GZd177Zu3vH0g1ffO5tQ5HbWp6ltIzMgcwnxtvv9+2NA5CT+vPKG24uyszOGacq6lmkKOnEHDYkH/H5mj5/7/U52xItX4+ZeIFt0OAhwy0221gB0QfE7ohVyw2GYexrrG/8avHbb0Q5JJw9bIT1/KkXvgBASuaIexox7+meR374VzD8r7745/9Motwm/xtDDDMTETlxcnImrP50ydFbZv/gADOCzDJo8lrxu6DFOaPPsw0+Z+RZDqd9IkjpJ8BOyWiS0tjTUFe7cvE780rjFfjex378V5zYjiHX/2PV40NGjilyudNmCkJPXRr73njx2edmPfDoHarFMsm8X2luanxzwRtz1pncz3iPJ+0OEEkA0LTw2oaa2q+y83IvVlRlIEBWKeXBhrrapR/Mf3N/rCyfN3HyiwxoMlaWAeWeR374N5Zc/dpf/vdXMfovk9AASTmQtkAkyomIPbv3ePfs3rPK7XZ/ZXfYrTXHawBAVRQlNuHPSQBSVVUV+OXPf/V7lhIej0s1DFaCwSDLE4EhycDDACDTMzKntDSKQY0N9Ssb62qMcChUb7XZFWZmIoInLa07gCNxZhhFFPKWgu75+bNBSG+hCIEQAIeqWsZ279Hr3Gtvu/Nvi9564wAADBo6bBAze00SR8QEtwGSgz5f80YAilBEBktpLtly4OCe3cdi+lTcdv9Dt1ittmEEKCCKrofrADmFqo7Ozus+8vb7H9447+UXF8SOg8vl6slgDYDBDGFIwzt+6oVPgCifiCjCZsswA1bFYpkweNiIgm3fbPhz0B+Iz7OSkntktdqKTPeQQBDlJcX7k11rEYJuvu/hRywWtXeLkEkZZgBCUKHHk15450OP7nvjL8+/FDtLTr386ozCAf3+I8qtIEKK6SbD4lYUdUxWTvaY2+5/YMVbL//tUwAYMmJkP8kciFAxLceHRMciGA6HtsTxICe5GYGAv8nhdOoArAAgFGXgrfc+cPdnSxa/U1NVacSC6ttzXno9biaOdR8wYdrF7kGDhz5CgrJj5ChIBKsgZWRWbt6Im+66b+67r//922jbZ1x3U34kApv9iGxcrT1n9Njxdpd7ZmROQlgLhY4AUIgoG8y+aDurqyqPRYHHarEOMCczJiIIRemf0737SAKUSAZR1olE38zs3IevvXXWK4vmz90NAGcNHnK2ZG6OynJ8HwaC/s04OVshJwIR0Y6fxgmWTw2nyyU9Ho8BwHC53bJXn369R4yaePFrr7/19D/+8fn9T/3sZ0X53bsriWIUVFXFD594oujtt966Y+4bbz130SXX3tOrT78B2Tl5rujzVdXCcaDRAh4zb71jhKIofbhlpYC1bZs3bQZAPr/vOJtoysxkdzh7oHUWdwFATLtsRn63/PwfMZAeMdEhNU3bb+jGMTAr5tISpWdmPjhs1CgXAEEgmxYO72Bmf0tnM6CFwkdCoeCusmNHtw44e3AGGM4TmeoNr7ex0Yi+96a7759ptVhHgVkxQ/5Dmq7t0zRtDzOHwUxgFlarderN99x3EWIyultt9sERjGABZigkBhBRX2ZZHw6H9uq6XhZNcG3mXxkwfuLUfCTOah9vDrca875FZ1mEIpwtX0gOVldUNCQDkBtn/+A2VVX6mu0Wuq6Xh7XwXmYOs5TEUpIi1IFX33L78Gh9evftZy/s1+8RgFzm1iep68YRXdN2GbpeaY4DgVnYrI6rp8+8oR8AYeiGTQuHd0gpvWZktGAw6ZpWHA6Fdh2vqPwWbad6pA/mz60ydP1QxI1pAczBV1x742+vv+OeGeOnTC1IhdAdN2la5sDBQ34OohyzDdA07aCuG0eY0SJHTo/73vMmTc6MukjOjPThzCxN2ROGbtTZHM6rwayC2cpSisqysu0F/fo7iSgt2k7JMlR3vDoUbZtqsQww46ZIsiQCsnRNKwmHw7ullAFmVsz+R1pm1t0ZGRlWAAoY9lhZliwFM5MW1g6HQ6Fd5ceO7UwiMyctg6sp+t8tpmBBYX8UHzlkjBg9NiczO+cOgC5mls6CggLr+HGjKBQMFc6cOXPijBkzAqUlJWseePCR+ZVV5RIAXvrb364sLCy8RhHCQ0QqEVDQp9eFdY3Hp+b37BVk5rLGhroF2zZv2ux0uREKBqRhGNEVGB4w8GxbRmb2jWCEIvUja2NdzaeH9+3xAVAaaquP53XPF5E4emKr1dYjxuQUAGR+r172gv6F95mmL0BExUeOPLfy4w8qLFaruPnu+2arqmVgtP1FZw8bvGPLlq0L5s7ZmpPXbecV1930IxLkNklbbee3mz/c9s3GegA0evz5eUSwR8HN0A2vmeVMXDrj2oEOp3OyaUUQM5QDu3e/uPGrz2t0TeNho0anjR4/8SfmmITsdueFw0aO2bJj6zde06w7m6Nn7piD6Gv2Llvy3jurA36fBIBZDz76SyGEIzIhEbvTPXkAqhOMr2xjskCv3n0cBHK2HP/FsqnZ2xRKtBR99Y23nmO12UYys04goena/nkvvfgKALr6pttGZubk3BLxuFikedInAtgLgPuddVYOE7LJtCJ0XS+ZP+dvr+iaBqfLJWbcdNs1Npt9oK5rFVo4XOkP+CQA5YO35+4DsG/WA48+LhSRYQIcHz144OMvV68oT0CcJowGfvv1Oa/cNOvuWYqqjo51yZxu17iiQUPG9j9rsK+itGTxqqUf7U0UQ+Jyu5WBQ4feTiA1ulJzvPr4q0sWzNsDgG6778GbrFbbuSYBy32LBk3YsPaL5RHLwTbM3OtBzAxFUc4CoHqbGj/du2vnlpy8bulfrvz0eNHZw1wgZLRMlpIDlWVlQQAiJ7+bhYhywQhE2+xtalz6yaIFXwT9fpnbrbt1+nU3PkVE0URfyrgpFw9Ytvi9gwvfev3brJzcnVdef/MTQhEtsrx729aPtm7D8EFjAAAY7klEQVRcXxtnabVJEKspkniYOO0SslistH3zRgkAmVk5Mwg0kyOzMzU0NuGLtZv4vHOHkaHrgpldPXv2vnLO67dPr6zZWGJTC3vn5xRSZAJlEAHlFTVUVVUHQUIws5OIitLSsy4H8I3f16y5PWmkaRqZhJgcPe78SQDECQXVqxa/+9b6KEru3bGzauCQc0SUWVWtlu5xs60YMXZCIYEyWEoiIg6HQntWfvxBFQBFC4dRW3t8fZono1zXtWZN03wVpaUt5KHNbreQoCyzw0lKGS4vKW7JSJaZlZMHkAqOLEuF9XCDr9nLAERufv61LCMvBWCtqah4bd3nK+qi9+7Zsd0/bNS5+61W2xCzy+15PXrkYSv8VoddQIh8Bgei7QiHQrsWvDFntfm70jJm5jI2A9i6afORmL+luqrDnowMJxE5okvuhiG9TScO+moBEaEIZGTnXsvMLYFbKxYvmhd954fvv7vjrh88PIuZNSJiRVEHmDInwbAgkiuGzaXkbpfPvGH0kf17j+z8dkvTO6++/FECcFMQyT5HQhHdY1bfjNLio40J+ImkpGUo4OO5L70w75pbbt/l9qRPVFV1QIvCEJFQyN2rb997b539wP6d27e8v33TxppYEBo+5rwcIUTflu0aUlat+mTxgWjby0qL13fv3tOv65pX17Tmhob6cgBCESopitKTIyEM0RVDPnrgwIufr1haYlbPC4C65ffIAcgecXFBuqH7qivLDQBi1NiJgxgszfEmw9CPL5z3ejScQDleVWkEfP6tTpdrXFT5bTZrtunSR2SZKIdlpA5SSq2s5Ggz2t7Fjo4ASKt13y9Xf2YA4Nxu+UrPgr4UDAVXORzO2wmwmFJJm77ZjsKCfLhdDiiKA7XNH6HJv0XY7WqfoL8EtU0LkZN+I8LhSIzVxi07YUgjGlMAACg5eugV1WIhIqJmb1M4NsLU6XJPia1ZTXXlqliftLGhXo+QXSQiwWTCkZ2Va62tOx6NBREZmRmjGOZaP0P4vE27Y4mzpe8v2AdgX6LlsKKzh3YHyAHTijCk4TteValFOzktI2NATP2UQHNzMSKh0nmChNMM1ycpZc3KTz9qFQykaxoMXfPzicRCqhpJ96hccOGlRTEzFoHI+uE7c9+NBYfBw0emgWE7MVsZ9TVVZYlWCLid4DWkp2f3iZCNxEwsDF2rCgWDRrxCX3DJFb1InJAhXdcrqqoqtJb+NHRiRNw9ZiZCy/GnVHO8OtS36Cx/JOUlCIA7Oyf3xqzsHB45dnxDzfHqNZ9+8N63iSI/x5w/ORcgF5jDzCwkc+DowQNBdGI/yuK3521zuly783r2co2dMOkqh8M5hhENWeKgarUUnDN67EPbN238TaxC9enX79yYGVr1+307/ZHTFwUAfP7pJxUAyuP6XZw3eWquOUbmOJO1sqJ07ucrlpbFrH4AADJz84ZEzn5lAqAG/b7D0fenp2cMYo5wVASyVJaVLounJMLhkNfhdFLMoDmiMlNYNCgfBAfzCVmurqjQE1gdSLJyxqkAyEkCdryqQgIwyoqPHpp22VXFLGV/AGhqqmOrxUabtuzEtMnnQSgaGr2biRkQwgpFFRw2GmF3MATZsWnLDlRW1lBdfTUkS1AkkKy0sqK8Qde0+KU33Hrfg7ci4h4YUarKZnekX3XDLRNNa0AwS93Q2aeolBa5j+VZ5wzrtX7N6kPRjlct1qIoJDCgVpSUlMatsccvN7YIZV5+98FglhzN0ObzHYhFaLvD2d+ckYiIlMrysmIAlJ6RmRntawaTFg5V6Loez0FxXLg4G5EdiMKTkTmAmQ0GCwJR0O/b6mv2x677IysnJxsENQoguq7XxORdTTQzJ01/YLPbzopdLfV6vYcT8WKetLTs2DooitLzrocefzYmBJ6Y2R/TwqiA8q5vtzT3Kxq0LCsnZwYR2aPtAwBSRG5e9/zb7/jBI1fVHq9avnTRextj39uroGB47DgEIytEbQXqtQmYfp9PHt2/z390/773+gwoWjp67PiJ7rT0KdHREEK4r591z/T35776SbQfLVbbMNPFI4DUusqqAwnk6KSSkZWZ23INgwxDr172wfu7YvqxZVwcDseglsBLglJTVXUwep3VausT1RFm1vbt3FEaDyBCCEfsdgTD0Fv6v3uP/KHRLfMUkeX9HYhdaZNERTvo0zIYuhZ+NnZV3mK1Y9vOfTh0tARCcVKW5yIoihWKYocQFnI7+pMq0qnR20zbdx0kq8UKRajmYgpIC4e/DQb8WvwgTJx6cQ+r1TqMY4LGEDlK8rLM7JzLM7NzLs/Ozb0sJ6/blYoqPNH6MSCzs3J7xs4cCinZLc9gFhu+XFuThCg6qdjsziEt/UBQKivKWgmuqqo9Y4Hnm3VfVgIgRVVt0RUjANB0zWvoeqv1fUVVhVAUZ8uMDeaA3xcGIGw2WwG3nNvHoqqibHM8IepJz8iMtUhCoVCNCVJtmfMJ26sqysAYAFcOHdh7OJEloKiqNV5WmDnEzCGWHGbJITCU6Ec39LrYei95b/6WTxa++3tvU9PnHNkLFVUsMHOYiBy53fKvn3rZ9EGx9zlcnqHMLXsflOPHq3anGOHL7cRN8LGDB/yL5s9dUXrsyJtEZDEbZTgczqGxfakIJTf6DAKUjZGxTkZS44QV7cqLygIzUzAY2J/setWiFiCS4IIAiC9WLi8GgJ4FfZwkyB6RYZCURiAUCnH8+61WW7fYlocCQX90QrE73YO5pQ9Iqaos34fU8s6ktBs3peCmL1Yt3zrt0iurmbmboYfQ3FwHu9VDR46WUv/C3vA4zyN/aDt06YciGDlp08AwUFpWDWkwQpofYS0QdWH4wN7dHyXwvahX38ILYiJMKWYVhNpMX8iQdqerR+yzmJhiwt0Fw2gvwxMDgCc9XVEUpSdMkw+AXL9mVXG0XhOmXtg3GmxHRKRp2rGEShoZ9IRRmqqqxgw4G6XHjtZl5+ZahVA80TpL5lBlWXld/GzldrsLYp8ZDgYqOhNLPWHKRS4IyoDkoEmg+vdt39aYIDiMWUottl1NDfWfLZo/94v4eIokVh0AcE11lbZw3mvLAXw29bIrB2Zk55zldrtGAmSPTpw9e/eZCeC/AZDVbhOqovSKKhYx8T+WLT2UBDhagcjNd99f6PJ4HgagMNiqa9reuX99/u8JFIb27dlV3LuwX8sEKyN7NWKVXDkR0w/yehuMVKKNrTZ7L8RYms3epuJE10288JIeBDIipBbB0PUoP0LZOXkuIoo91D66f6nV+602a/8YKaOSo4crENnHpVhUtUfUfQFBrlu9srg9cO0MB9KuVaLr2gpFUW9nAE3eRjhyPDh4uAQTzjuHbFYbiBhC2EgIibDuha4LHCsuhWSJxqa6aHoQ6Lq2seZ4lT9e4YaPGZNldzjGghFstYrALFriUE9WVBHVQqvVmhMLSNKQjSTISZEkyOzJzLZ662s1AJhy8fS+BQMGzDJ07aim61UBn690yYL5WyIDemn/2EA6LRQ+GtsXud26x/4dfm9z1CRkv8/XbMammOkfrd0URRFGJBEsANC48yf3Uk4MqggGAgeOHTro79NvgEsI8pzAFRloqKsNxo+HxWbvc2JGJNRWVZV1ZmD7DOg/0OR4AEAYmrY3GagG/P66zKxsw+SUYLHZe8XPpFabTRi6Ht0TxRlZWer4yVOHuNxpPa12ew8iWD9ZuOClhrpafc2yj/cDOJCT23319Otu+B0zB5iZSChRMKGxE6cUxNZF08KlKcyaUZcwHAU2AnRFUXsOHDLMcfjAPk0Lt9pSxRdceNkd5lhETHx/855YOTOkUaMIJdO0uji/oK+jovhoEACNnzStW9HQoQ8ZeviYphtVQZ+v7KMF8zcCgKIqPWNfVFleVpHIws/OzSuIlbdgINjiRnoy0j1EZItaYUIo7oycXGd1ZUUoOqlcdcPN5xMJh0lwC10LH9u3a0djZPl5aiFzSxAmhcOhox2w0lrJgtoJy6PViwJ+/xq3J+12AkE3NPiDXjA8qK72IidvH0CSVGGDIRi+4C4IYxSOllRA04IIhQIgIiISXBEhgU56/vBRY+8yzVkAUELB4I63X33p7WRr1GnpGeqMm277KRGsADEJsmZm5Vjq62rCptBvd7nd4yOpE0m/ePqVUxa99caK86de1Lugf//bCLBbLNaBVqvtHF/kCMnIykSapy9ORL6CGf7YfnE6XX0ASHPHsVJdVb4v2tE7v91c2r9oYBjm2ThWm63f9Otvnrjk3be+AIDp1900Oic378qowLLk5hUfL3k/4jPnOIiEM+pSGFL6K8tLg/EDa7FaeoERNnkWZd0Xa8pS5QBaB5DZz2I+sf+publ5XxIXlteu/PTIDXfeFyaCEwDsdvvQy2feOOLTDxZsz8nLtU6+ePolbk/aOF3Xq3VdP7576+aljY31gdzuPa40oz8ZAF90xYxLN321dvWxwweD3Xv2sk+4YNpUINIWIhK+Zu+2FmIxKys/tt58YhmTVVUlPQJWCUHk2IH9NcNGnxtE5N0gIveEKRf+4tzzJx/Qdc3LUkqAXFabta8QSnY00pmZA2tXLFsV20/NTd4N6RkZV5hEZnjStIumL3j9lQ9Gj5uQN2DwoNsJsFgstv4WKw0KBQLzT/BEandExxnA1q/XVePkHCrS4XL2MuUJzAxvU8OhqLxl53brF+c3YvS4CbOkob9xcM/uhouuumZEZnbupSdWx1j/Zv26t6PvyMjK6g2CEV21k9IMzDPjtBL0YVLrXO2k9dHy4E3r1x688LIrK6TBPQiEZl8juxweFJeVIj33ECmKHUQWCJXB5MeR4sOQUsLb3NCy8mIYelVNdWVZfNTqtOkzBgpFyYqptLZo3qtvt8UO2xwOwSx9RIrFFBJbZk6Otb6uJgQAhw/s3Th0xOhRIFgZzJ609Cl3PvjYhfHvDvj9//hk0bu7ThBSqjPKvhBIWm3WgXc88MivWEpt3st/+b9CUT0n6sHB41WV9dHfm+rrtbKy4gW9CwrvjkaTZmZlXXLng49eHuVqWjgHhn708MH59bXVWsTfLSiMjWTVQqF4YJATL7y0IHLkRWRCkoYsb8cUTQgoLleaUBSlZ0zELleXlx3Cybu0OWJZ+WVZ8dGXe/cp/BETG2DIvO7db7jzwUdvNpVbApCqquaxlI07t22pA8D1dbXLsrJzZkbb7HJ7xk+5dPr5QItVKU3FhJSyecmC+R/HKKAtxipnq9Xa944HHvk1GKE3X3rht22sGmDjurWh/mcPfsfpcv0AgG5GcKqqRR2iWlTEusQtnBMzl5cUv3+8uqpVyoitG9dtvODiyyYRCSczS4fDOebOBx87L06OEA6Fdn320aLNAHDZNdcWxY5ljFvSalwcTidZVGumOaRERFpjQ31N9FqXy13YGkRZUxU1e/zkaf854YIL0Wr/D4Eb6mpX7N+9oymmD50xXJ602W1n3fHAw79iidC8l1/8XarykiqJ2hY3IAHIYCDwPMxcH5qmodnfhJKyA9D0EKwWOyvCDkWxg8HYd/AQpKFB00ItRL2u6zsaG+p98Xtn8nv2uoRAiOYR8fma14bCWnyIfKvt/gG/PwzAR4IkEbEQwuL2eCzRe7Z8va6qsqT4HTB0opbNhLrZmRYAoqmpccWH78z7ILbhVWUlGwCyAKREORBBwhIKBnf3LuznEELYTZqUpWSvt6kxFKtwqz9ZsqeqovxNZkgisphn3OgA6WYbrYZuVBzat+eVtSuWRcPvOS09owgECQITkdLU0HAoHuzye/Q4C4AR2axIFAz4d8WFr6c0o4w8b5wdgCvm7Bqft9kbwMmJnFr+v/LjD0vqaqpfBUOSEOZKE3SO1EcBkeptbFjz9qsvvRq9b8mC+V9XV5bPA3GQiKzmRCIZ0EEwiEBCCJuuaWU7t37zUjgcjpryXHLk0C5mCJw4koQFCVXXtENIIdHP23Ne2lFfU/ec1GW5aYmodGJ7TFQnVCKyGZpWeezwwTkrP/loV/yzjx484C85fHguswyYZCvhRMY8lYiU5mbv2qUL333DPMMH6RnZZwMwInJCIuhr3o2TU1PKjKxsIUQkSA4EZpahhro6X4uVYLH0a3FXiWw7t235u2QZAkFEIlwjpygAoLqamk8/evftL2KfX3bsyCaALAQSZFJrghRVC4d2J5GXpCknTsUCaQGRY4cPflt09tAjAOcAEM2+Rqqs8FAoaCG71UIMlcASQb8wqqsaGpt9jWyuEEBRVHfxkYMfxux50QEYF8+YOVhRlDSAfWwySVs3rPsiboPUSaRVc1OjbhhGnaKoaVGCye5wKjH+JH/2yYd7XC7PLy+bed1Uu8M5WAjhZslNgYB/71erP/uisrwsFB80tWb50rJB5cVPjRo36SpVqIVMbGPJgYry0k3ZObm2CD0RWbI0DL2mobY2hNZZ0njZ4ve3Adh2zc23jXO6PcMVoWRLIKBp4aqqsrKNn3+29PBJ6+xWSw+W3GwKk35g76798YNsdTh6M7gJDCYi9XhlxS60n/bwJGFIz8q0EBFYSj9H9mjU1VZXBnFysqBWMvDhu/N3APjp9bPuvtDucA4jRjqz9AUCgX1rln7yeV3dcS2uLvj0g0hfXHn9TcPT0jOGK6qaTyTsUhrBcDhUVlFaunntyuVH42fozV+vqy4rOfpfky+58kqb1VoIgo0lArU11d8gcaLpkwR/0fzXDwJ4Zsoll+Xl9+4zTlHU/kJQBhMJ1qUvFAoUV5SWbP1y9YqyBBZNy7NXL//kCIBfX3vrnROdbvdIIkpjyb5QMHBwy4Z1/zi0f29sFjNY7dYCZuk1jx6wlpeV7k707PTMTBJCKACaIyEK7C09etgLAKPGTsgVQqjMbBARDGnUfLthfcW3G9Y/PePG2y92ezwDiVgJBUMHN3+9btWRg/u88eP99drPy6oqS39x7sSp020Wa18IskgpAxUVpRuRPLNcwt87k3OSYmILFBOELDEfa8z/1ZjrEBd/EJ9pzDB3O0Y30GlonUgoWXRcsm3UybY+JyNekcC64gSrB5Rgfw2S7StJ4b2nch6NRPv5R2XMBsXYrG6JkgK3ygeSgGPiJAImEnwS3ddW3tb4fu1ITAd1oK3J2inixrIjR2kkW7ptL6kStTOeFFdfcfVNt03KyMqewWADDMXn825a+ObrH7Wxfy2ZFdFW2ol4y95IZsl21gIhJE57KHAilV/sHhrRhtLHVlaP+8Sf85rsHBduB0DaS+hL7QhrokFttQTbBlC1peDUxvtTjc1pL+uXTKL43IaSRjmQZJGs8RmqEglXsvFuC5Tj+5PaWQmgFNqazG1Lmq2snfgRmaQOog15SyYD3I68nTQJOj2eflHLlgjUUFOzNwm1kGrS7VQnT5xqHEiyrf6xKQ+1BMAikswqsc8w2vh0RJmQghWSKoCgndmmrT0XHQGQjmYTby9BD3ViNqIkQpTofJtEAJLI2mgPQNDGDJ7sNL72wLUjSsPttCeVa5FgIqEU5DPVCaFVH3rSMxSLaskiImketKaVRSKpZSJusp2+aO/Qs5SAp7OBZPGsPMVyDDGgoCB5NiVOwWSSHeh4dBJAKIVZAe0IfHszF9Cx4ytTOfEu1aTQyWbOZDlhY/NnCJyczUwmMYWj14gUrIi2XM62FDhVAOH2eBAk3mWa7CAvpAAglOKEwClyi/GTlRhw1kBrlKynCFvv9fuaT+LrEpHdnQAQTkUHOxtIBiRPcxYPIG11bKJT1mSCinM7A3sqFkhHzq/tyE7Ftky/U3FhOEXrqz3Xob0xbW9TVTIF4nbGO1XAp3bemSqIxFtFnMDlEW24DdyJSaUjFmV7XFnLMzNzc+0khANsxmIzN9XWVAdxctrBVAEklXOH2pSd05FW/ySiJ45gbE8okAA1uQMmK9oZ0I6AR1tEaFt90Nn6tTUOqbpuHQGQ9hSRUlCGjlhlQGrnlaRCKrcHlkDqR3dQO5ZPR/qwvZP8uANjm2yyOikpVhJuUabgwrQF4EiBl2l1/6kcfpNsAEQKg5JKZbkTitQZgKTT8GzqIHi09QzuBJh0hHDtyLi2Z/Wlel8qbaFT6MvOuI/JgKQjFkJHALCj1mV8PRMBSKKd1e1ZH9wJuUna9tN1JiylSIa1tdnoVIGDU7AsUm0PpziwHQUgTrF+3AmAwCn0V0f7rLNWRGfHqqMAmqoMdQb0TseE0FFgTWTpx4cQJAKQtlbeGB23nk+bC9NWp1EnlaSjx/LxKdSzs7P4qTzrTBx92FnrhM9AH6GTAHI639vZ/qDT9Gw6zfVua2JuLzSiLWueO6ibSdt+pgQm1dwTp9Nc5zMkkP8uhc8gAHyf+5zPkMJ/lyXeohftuJiyA7zXKfUhnYGGft8UgrqA47SN5+nscz7D48NnSG7PNCBxOxNwR5a7+RRkgL8PykXfkaB3la7y71ooCYi0ZdWnxF+cjqJ0jU9X6Sr/UiDS1qTL7XzfBSBdpat0AUnKVjz/syvUVbpKV/nXApHvlBLoApCu0lX+fcDkO+cR/z+k6qtOLHRjXwAAAABJRU5ErkJggg==\"/><br /><p style=\"margin-left:10px; color:grey; font-family:arial; font-size:medium\"><span>ATS version : <i>" + atsVersion + "</i><br />Android version :<i></i></span></p></body></html>";
            return new AtsResponseHTML(htmlContent);
        }

        JSONObject jsonObject = new JSONObject();
        
        try {
            jsonObject.put("type", req.type);

            if (RequestType.APP.equals(req.type)) {

                // refresh applications
                loadApplications();

                if (req.parameters.length > 0) {
                    if (RequestType.START.equals(req.parameters[0])) {
                        if (req.parameters.length > 1) {
                            try {

                                final ApplicationInfo app = getApplicationByPackage(req.parameters[1]);
                                if (app != null) {
                                    appInfo = app;

                                    executeShell("input keyevent KEYCODE_WAKEUP");
                                    executeShell("input keyevent 82");

                                    device.pressHome();

                                    final AccessibilityNodeInfo rootInActiveWindow = automation.getRootInActiveWindow();
                                    if (rootInActiveWindow != null) {
                                        final int childCount = rootInActiveWindow.getChildCount();
                                        for(int i=0; i<childCount; i++){
                                            final String viewId = rootInActiveWindow.getChild(i).getViewIdResourceName();
                                            if(viewId != null && viewId.contains(":id/notification_panel")){
                                                executeShell("input touchscreen swipe 930 880 930 380");
                                                break;
                                            }
                                        }
                                    }

                                    executeShell("am start -W -S -f 4194304 -f 268435456 -f 65536 -f 1073741824 -f 2097152 -f 32 -n " + app.getPackageActivityName());
                                    sendLogs(app.getPackageActivityName());
                                    reloadRoot();

                                    jsonObject.put("status", "0");
                                    jsonObject.put("message", "start app : " + app.getPackageName());
                                    jsonObject.put("label", app.getLabel());
                                    jsonObject.put("icon", app.getIcon());
                                    jsonObject.put("version", app.getVersion());

                                    activeChannelsCount++;
                                }else {
                                    releaseDriver();

                                    jsonObject.put("status", "-51");
                                    jsonObject.put("message", "app package not found : " + req.parameters[1]);
                                }

                            } catch (Exception e) {
                                System.err.println("Ats error : " + e.getMessage());
                            }
                        } else {
                            jsonObject.put("status", "0");
                            jsonObject.put("message", "start home");
                            jsonObject.put("label", "Home");
                            jsonObject.put("icon", "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAAH0lEQVR42mNkoBAwjhowasCoAaMGjBowasCoAcPNAACOMAAhOO/A7wAAAABJRU5ErkJggg==");
                            jsonObject.put("version", "0");

                            activeChannelsCount++;
                        }
                    } else if (RequestType.STOP.equals(req.parameters[0])) {
                        stopChannel(req.parameters[1]);

                        releaseDriver();

                        jsonObject.put("status", "0");
                        jsonObject.put("message", "stop app : " + req.parameters[1]);
                    } else if (RequestType.SWITCH.equals(req.parameters[0])) {
                        if (req.parameters.length > 1) {
                            switchChannel(req.parameters[1]);
                            jsonObject.put("status", "0");
                            jsonObject.put("message", "switch app : " + req.parameters[1]);
                        } else {
                            switchChannel(null);
                            jsonObject.put("status", "0");
                            jsonObject.put("message", "switch home");
                        }
                    } else if (RequestType.INFO.equals(req.parameters[0])) {
                        final ApplicationInfo app = getApplicationInfo(req.parameters[1]);
                        if (app != null) {
                            jsonObject.put("status", "0");
                            jsonObject.put("info", app.getJson());

                        } else {
                            jsonObject.put("status", "-81");
                            jsonObject.put("message", "app not found : " + req.parameters[1]);
                        }
                    }
                }

            } else if (RequestType.INFO.equals(req.type)) {

                try {
                    DeviceInfo.getInstance().driverInfoBase(jsonObject/*, device.getDisplayHeight()*/);

                    jsonObject.put("status", "0");
                    jsonObject.put("message", "device capabilities");
                    jsonObject.put("id", DeviceInfo.getInstance().getDeviceId());

                    String model = DeviceInfo.getInstance().getModel();
                    if (model.startsWith("GM")) {
                        String[] parameters = model.split("_");
                        model = parameters[2];
                    }

                    jsonObject.put("model", model);
                    jsonObject.put("manufacturer", DeviceInfo.getInstance().getManufacturer());
                    jsonObject.put("brand", DeviceInfo.getInstance().getBrand());
                    jsonObject.put("version", DeviceInfo.getInstance().getVersion());
                    jsonObject.put("bluetoothName", DeviceInfo.getInstance().getDeviceName());

                    loadApplications();

                    JSONArray apps = new JSONArray();
                    for (ApplicationInfo appInfo : applications) {
                        apps.put(appInfo.getJson());
                        /* if(appInfo.packageEquals("com.android.chrome")){
                            appInfo.setChromeDriverActivity();
                            apps.put(appInfo.getJson());
                        } */
                    }

                    jsonObject.put("applications", apps);

                } catch (Exception e) {
                    AtsAutomation.sendLogs("Error when getting device info:" + e.getMessage() + "\n");
                    jsonObject.put("status", "-99");
                    jsonObject.put("message", e.getMessage());
                }

            } else if (RequestType.DRIVER.equals(req.type)) {

                if (AtsClient.current != null) {
                    if (req.token == null) {
                        jsonObject.put("message", "Device already in use : " + AtsClient.current.userAgent);
                        jsonObject.put("status", "-20");
                        return new AtsResponseJSON(jsonObject);
                    } else {
                        if (!req.token.equals(AtsClient.current.token)) {
                            jsonObject.put("message", "Device already in use : " + AtsClient.current.userAgent);
                            jsonObject.put("status", "-20");
                            return new AtsResponseJSON(jsonObject);
                        }
                    }
                }

                if (req.parameters.length > 0) {
                    if (RequestType.START.equals(req.parameters[0])) {

                        String token = startDriver();
                        jsonObject.put("token", token);
                        jsonObject.put("status", "0");
                        DeviceInfo.getInstance().driverInfoBase(jsonObject);
    
                        AtsClient.current = new AtsClient(token, req.userAgent, req.atsVersion,null);
                        sendLogs("ATS_DRIVER_LOCKED_BY: " + req.userAgent + "\n");
    
                        if (usbMode) {
                            int screenCapturePort = ((AtsRunnerUsbTest)runner).udpPort;
                            jsonObject.put("screenCapturePort", screenCapturePort);
                        } else {
                            jsonObject.put("screenCapturePort", screenCapture.getPort());
                        }
    
                    } else if (RequestType.STOP.equals(req.parameters[0])) {

                        stopDriver();
                        AtsClient.current = null;
                        activeChannelsCount = 0;

                        sendLogs("ATS_DRIVER_UNLOCKED\n");

                        jsonObject.put("status", "0");
                        jsonObject.put("message", "stop ats driver");

                    } else if (RequestType.QUIT.equals(req.parameters[0])) {

                        stopDriver();
                        jsonObject.put("status", "0");
                        jsonObject.put("message", "close ats driver");

                        terminate();

                        return new AtsResponseJSON(jsonObject);
                    } else {
                        jsonObject.put("status", "-42");
                        jsonObject.put("message", "wrong driver action type : " + req.parameters[0]);
                    }
                } else {
                    jsonObject.put("status", "-41");
                    jsonObject.put("message", "missing driver action");
                }

            } else if (RequestType.SYS_BUTTON.equals(req.type)) {
                if (req.parameters.length == 1) {
                    String button = req.parameters[0];
                    try {
                        SysButton.pressButtonType(button);
                        jsonObject.put("status", "0");
                        jsonObject.put("message", "button : " + button);
                    } catch (IllegalArgumentException e) {
                        jsonObject.put("status", "-60");
                        jsonObject.put("message", "unknown button type ");
                    }
                } else {
                    jsonObject.put("status", "-31");
                    jsonObject.put("message", "missing button type");
                }
            }
            
            else if (RequestType.PROPERTY_GET.equals(req.type)) {
                if (req.parameters.length == 1) {
                    String propertyName = req.parameters[0];
                    try {
                        String value = Sysprop.getPropertyValue(propertyName);
                        jsonObject.put("message", value);
                        jsonObject.put("status", "0");
                    } catch (IllegalStateException | IllegalArgumentException | JSONException e) {
                        jsonObject.put("message", "unknown property");
                        jsonObject.put("status", "-1");
                    }
                } else {
                    jsonObject.put("status", "-31");
                    jsonObject.put("message", "missing value parameter");
                }
            }
            
            else if (RequestType.PROPERTY_SET.equals(req.type)) {
                if (req.parameters.length == 2) {
                    String propertyName = req.parameters[0];
                    String propertyValue = req.parameters[1];
                    
                    try {
                        Sysprop.setProperty(propertyName, propertyValue);
                        jsonObject.put("message", "set " + propertyName + " value");
                        jsonObject.put("status", "0");
                    } catch (Sysprop.BooleanException | RemoteException e) {
                        jsonObject.put("message", e.getMessage());
                        jsonObject.put("status", "-31");
                    }
                    
                } else {
                    jsonObject.put("status", "-31");
                    jsonObject.put("message", "missing parameters");
                }
            }
            
            else if (RequestType.CAPTURE.equals(req.type)) {
                reloadRoot();
                jsonObject = getRootObject();
            }
            
            else if (RequestType.ELEMENT.equals(req.type)) {
                if (req.parameters.length > 2) {
                    AbstractAtsElement element;
                    String elementId = req.parameters[0];
                    
                    if (elementId.equals("[root]")) {
                        element = rootElement;
                    } else {
                        element = getElement(elementId);
                    }

                    if (element != null) {

                        if (RequestType.INPUT.equals(req.parameters[1])) {

                            jsonObject.put("status", "0");

                            String text = req.parameters[2];
                            if (EMPTY_DATA.equals(text)) {
                                jsonObject.put("message", "element clear text");
                                element.clearText();
                            } else if (ENTER_KEY.equals(text)) {
                                jsonObject.put("message", "press enter on keyboard");
                                enterKeyboard();
                            } else if (TAB_KEY.equals(text)) {
                                jsonObject.put("message", "hide keyboard");
                                hideKeyboard();
                            } else {
                                element.inputText(text);
                                jsonObject.put("message", "element send keys : " + text);
                            }
                        }

                        else if (RequestType.SCRIPTING.equals(req.parameters[1])) {
                            String script = req.parameters[2];
                            ScriptingExecutor executor = new ScriptingExecutor(this, script);

                            try {
                                String value = executor.execute(element);
                                if (value == null) {
                                    jsonObject.put("message", "scripting on element");
                                } else {
                                    jsonObject.put("message", value);
                                }

                                jsonObject.put("status", "0");
                            } catch (Throwable e) {
                                jsonObject.put("status", "-13");
                                jsonObject.put("message", e.getMessage());
                            }
                        }

                        else if (RequestType.PRESS.equals(req.parameters[1])) {
                            String[] info = req.parameters[2].split(":");
                            List<MotionEvent.PointerCoords[]> pointerCoords = new ArrayList<>();
                            for (String pathInfo : info) {
                                MotionEvent.PointerCoords[] coords = parsePath(pathInfo);
                                pointerCoords.add(coords);
                            }
                            
                            MotionEvent.PointerCoords[][] array = pointerCoords.toArray(new MotionEvent.PointerCoords[][] {});
                            if (array.length > 1) {
                                UiObject object = device.findObject(new UiSelector());
                                if (object.performMultiPointerGesture(array)) {
                                    jsonObject.put("status", "0");
                                    jsonObject.put("message", "press on element");
                                } else {
                                    jsonObject.put("status", "-1");
                                    jsonObject.put("message", "unknown error");
                                }
                            } else {
                                jsonObject.put("status", "-1");
                                jsonObject.put("message", "not enough touches");
                            }
                        }

                        else if (RequestType.SELECT.equals(req.parameters[1])) {
                            String selectValue = req.parameters[3];
                            element.clearText();
                            element.inputText(selectValue);
                            wait(350);

                            UiObject2 list = device.findObject(By.clazz(ListView.class).focused(true));

                            if (list == null) {
                                jsonObject.put("status", "-3");
                                jsonObject.put("message", "List view does not exist");
                            } else {
                                List<UiObject2> children = list.getChildren();

                                String selectType = req.parameters[2];
                                if (selectType.equals("text")) {
                                    boolean success = false;
                                    for (UiObject2 child : children) {
                                        if (child.getText().equals(selectValue)) {
                                            child.click();
                                            success = true;
                                            break;
                                        }
                                    }

                                    wait(350);

                                    if (success) {
                                        jsonObject.put("status", "0");
                                        jsonObject.put("message", "press on element");
                                    } else {
                                        jsonObject.put("status", "-3");
                                        jsonObject.put("message", "List item does not exist");
                                    }
                                } else {
                                    /* int selectValue = Integer.parseInt(req.parameters[3]);
                                    if (selectValue > children.size()) {
                                        jsonObject.put("status", "-1");
                                        jsonObject.put("message", "unknown error");
                                    } else {
                                        children.get(selectValue).click();
                                    } */
                                    jsonObject.put("status", "-1");
                                    jsonObject.put("message", "Feature is not available");
                                }
                            }
                        }

                        else {
                            int offsetX = 0;
                            int offsetY = 0;

                            if (req.parameters.length > 3) {
                                try {
                                    offsetX = (int) Double.parseDouble(req.parameters[2]);
                                    offsetY = (int) Double.parseDouble(req.parameters[3]);
                                } catch (NumberFormatException e) {
                                    AtsAutomation.sendLogs("Error not enough parameters:" + e.getMessage() + "\n");
                                }
                            }

                            if (RequestType.TAP.equals(req.parameters[1])) {
                                element.click(this, offsetX, offsetY);

                                jsonObject.put("status", "0");
                                jsonObject.put("message", "click on element");

                            } if (RequestType.TAP2.equals(req.parameters[1])) {

                                List<UiObject2> list = device.findObjects(By.clazz(String.valueOf(element.getClazz())));
                                for (UiObject2 object : list) {
                                    Class<?> uiObject2Class = object.getClass();

                                    // Obtenir la méthode getAccessibilityNodeInfo()
                                    Method getAccessibilityNodeInfoMethod = uiObject2Class.getDeclaredMethod("getAccessibilityNodeInfo");

                                    // Autoriser l'accès à la méthode si elle est privée
                                    getAccessibilityNodeInfoMethod.setAccessible(true);

                                    // Appeler la méthode sur l'instance
                                    AccessibilityNodeInfo nodeInfo = (AccessibilityNodeInfo) getAccessibilityNodeInfoMethod.invoke(object);

                                    Rect outBounds = new Rect();
                                    nodeInfo.getBoundsInScreen(outBounds);

                                    if (element.equalsRect(outBounds)) {
                                        object.click();
                                        jsonObject.put("status", "0");
                                        jsonObject.put("message", "click on element");
                                    }
                                }
                            } else if (RequestType.SWIPE.equals(req.parameters[1])) {
                                int directionX = 0;
                                int directionY = 0;
                                if (req.parameters.length > 5) {
                                    try {
                                        directionX = Integer.parseInt(req.parameters[4]);
                                        directionY = Integer.parseInt(req.parameters[5]);
                                    } catch (NumberFormatException e) {
                                        AtsAutomation.sendLogs("Error not enough parameters:" + e.getMessage() + "\n");
                                    }
                                }
                                element.swipe(this, offsetX, offsetY, directionX, directionY);
                                jsonObject.put("status", "0");
                                jsonObject.put("message", "swipe element to " + directionX + ":" + directionY);
                            }
                        }
                    } else {
                        jsonObject.put("status", "-22");
                        jsonObject.put("message", "element not found");
                    }
                } else {
                    jsonObject.put("status", "-21");
                    jsonObject.put("message", "missing element id");
                }
            }

            else if (RequestType.SCREENSHOT.equals(req.type)) {
                if (req.parameters.length > 0 && req.parameters[0].indexOf(RequestType.SCREENSHOT_HIRES) == 0) {
                    return new AtsResponseBinary(getScreenDataHires());
                } else {
                    return new AtsResponseBinary(getScreenData(0));
                }
            }

            else {
                jsonObject.put("status", "-12");
                jsonObject.put("message", "unknown command : " + req.type);
            }

        } catch (JSONException e) {
            sendLogs("Json Error -> " + e.getMessage() + "\n");
        }  catch (Exception e) {
            e.printStackTrace();
        }
    
        return new AtsResponseJSON(jsonObject);
    }

    private MotionEvent.PointerCoords[] parsePath(String pathInfo) {
        ArrayList<MotionEvent.PointerCoords> coordsArray = new ArrayList<>();
        String[] coordinatesString = pathInfo.split(";");
        for (String coordinateInfo: coordinatesString) {
            String[] elements = coordinateInfo.split(",");
            int x = Integer.parseInt(elements[0]);
            int y = Integer.parseInt(elements[1]);
            MotionEvent.PointerCoords coords = new MotionEvent.PointerCoords();
            coords.x = x;
            coords.y = y;
            coords.pressure = 1;
            coords.size = 1;
            
            coordsArray.add(coords);
        }
    
        return coordsArray.toArray(new MotionEvent.PointerCoords[0]);
    }
}