package com.ats.atsdroid.utils;

import android.app.Instrumentation;
import android.os.RemoteException;
import android.view.KeyEvent;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.UiDevice;

public final class SysButton {
	
	public enum ButtonType {
		app, back, delete, enter, home, menu, search,
		volumeUp, volumeDown,
		deleteForward;
		
		public static String[] getNames() {
			SysButton.ButtonType[] states = values();
			String[] names = new String[states.length];
			
			for (int i = 0; i < states.length; i++) {
				names[i] = states[i].name();
			}
			
			return names;
		}
	}
	
	public static boolean pressButtonType(String value) {
		ButtonType buttonType = ButtonType.valueOf(value);
		return pressButton(buttonType);
	}
	
	public static boolean pressButton(ButtonType type) {
		final Instrumentation instrument = InstrumentationRegistry.getInstrumentation();
		final UiDevice device = UiDevice.getInstance(instrument);
		
		switch (type) {
			case back:
				return device.pressBack();
			case delete:
				return device.pressDelete();
			case enter:
				return device.pressEnter();
			case home:
				return device.pressHome();
			case menu:
				return device.pressMenu();
			case search:
				return device.pressSearch();
			case app:
				try {
					return device.pressRecentApps();
				} catch (RemoteException e) {
					return false;
				}
			case volumeUp:
				return device.pressKeyCode(KeyEvent.KEYCODE_VOLUME_UP);
			case volumeDown:
				return device.pressKeyCode(KeyEvent.KEYCODE_VOLUME_DOWN);
			case deleteForward:
				return device.pressKeyCode(KeyEvent.KEYCODE_FORWARD_DEL);
			default:
				return false;
		}
	}
	
	public static boolean pressNumericKey(int key) {
		final Instrumentation instrument = InstrumentationRegistry.getInstrumentation();
		final UiDevice device = UiDevice.getInstance(instrument);
		
		return device.pressKeyCode(key);
		// wait(150)
	}
	
}
