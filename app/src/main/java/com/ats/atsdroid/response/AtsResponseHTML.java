package com.ats.atsdroid.response;

import com.ats.atsdroid.utils.AtsAutomation;

import org.java_websocket.WebSocket;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class AtsResponseHTML extends AtsResponse {
    private final static String HTML_RESPONSE_TYPE = "text/html";

    private final byte[] htmlContent;

    public AtsResponseHTML(String content) {
        this.htmlContent = content.getBytes(StandardCharsets.UTF_8);
    }

    private byte[] getHeader() {
        return ("HTTP/1.1 200 OK\r\nServer: AtsDroid Driver\r\nDate: " + new Date() + "\r\nContent-type: " + HTML_RESPONSE_TYPE + "\r\nContent-length: " + htmlContent.length + "\r\n\r\n").getBytes();
    }

    public void sendDataHttpServer(Socket socket) {
        byte[] header = getHeader();
        try {
            final BufferedOutputStream bf = new BufferedOutputStream(socket.getOutputStream());
            bf.write(header, 0, header.length);
            bf.write(htmlContent, 0, htmlContent.length);
            bf.flush();
            bf.close();
        }catch(IOException e){
            AtsAutomation.sendLogs("Error when sending binary data to udp server:" + e.getMessage() + "\n");
        }
    }

    public void sendDataToUsbPort(int socketID, WebSocket conn) {
        final byte[] header = getHeader();

        final ByteBuffer buffer = ByteBuffer.allocate(4 + header.length + htmlContent.length);
        buffer.putInt(socketID);
        buffer.put(header);
        buffer.put(htmlContent);

        conn.send(buffer.array());
    }
}