/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.atsdroid;

import android.os.Bundle;

import com.ats.atsdroid.utils.AtsAutomation;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4ClassRunner.class)
public class AtsRunnerTest {
    
    protected static final int DEFAULT_PORT = 8080;
    protected AtsAutomation automation;
    protected volatile boolean running = true;
    protected int port = DEFAULT_PORT;
    
    public void stop(){
        running = false;
    }
    
    @Test
    public void testMain() {
        
        try {
            final Bundle args = InstrumentationRegistry.getArguments();

            port = Integer.parseInt(Objects.requireNonNull(args.getString("atsPort")));
            Boolean usbMode = Boolean.parseBoolean(args.getString("usbMode"));
            String ipAddress = args.getString("ipAddress");
            String serial = args.getString("serial");

            automation = new AtsAutomation(port, this, ipAddress, serial, usbMode);
            
        } catch (Exception ignored) {
        
        }
    }
}